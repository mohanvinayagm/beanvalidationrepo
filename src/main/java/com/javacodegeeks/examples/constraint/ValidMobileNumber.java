package com.javacodegeeks.examples.constraint;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;



@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidMobileNumberValidator.class)
@Documented
public @interface ValidMobileNumber {

	String message() default " Invalid mobile number.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
