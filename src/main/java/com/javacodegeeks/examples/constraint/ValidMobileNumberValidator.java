package com.javacodegeeks.examples.constraint;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidMobileNumberValidator implements ConstraintValidator<ValidMobileNumber, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		Pattern p = Pattern.compile("(0/91)?[7-9][0-9]{9}");
        Matcher m = p.matcher(value);
		return (m.find() && m.group().equals(value));
	}

}
