package com.javacodegeeks.examples.entities;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.javacodegeeks.examples.constraint.CommPreference;
import com.javacodegeeks.examples.constraint.UniqueUserName;
import com.javacodegeeks.examples.constraint.ValidDate;
import com.javacodegeeks.examples.constraint.ValidMobileNumber;
import com.javacodegeeks.examples.constraint.ValidPassword;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotEmpty(message = "First name is required")
	@UniqueUserName
	private String firstName;

	@NotEmpty(message = "Last name is required")
	private String lastName;

	@NotEmpty(message = "Email is required")
	@Email
	private String email;

	@NotEmpty(message = "password is required")
	@ValidPassword
	private String password;

	@NotEmpty(message = "Phone number is required")
	@ValidMobileNumber
	private String mobilePhone;

	@ValidDate
	private String birthday;

	@NotEmpty(message = "Communication preference is required")
	@CommPreference
	private String  commPreference;

	@ElementCollection
	private List<@NotEmpty String> mobileDevices;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCommPreference() {
		return commPreference;
	}

	public void setCommPreference(String commPreference) {
		this.commPreference =commPreference;
	}

	public List<String> getMobileDevices() {
		return mobileDevices;
	}

	public void setMobileDevices(List<String> mobileDevices) {
		this.mobileDevices = mobileDevices;
	}

}
